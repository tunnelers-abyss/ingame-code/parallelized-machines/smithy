-- ######################################################
--               Smithy start controller 
--         by Guill4um on december, 05th of 2020
-- ######################################################

-- this controller start the machine changing mescon signal
-- to digiline signal

if event.type == "on" then
    digiline_send("material","/start")
end
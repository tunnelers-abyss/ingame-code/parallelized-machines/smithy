-- ######################################################
--               Smithy Main tube 
--         by Guill4um on december, 05th of 2020
-- ######################################################

-- this tube take stuff in input chest and send them to machines 
-- when they are free

--material1
-- LCD1
-- injector1

--material2
-- LCD2
-- injector2

-- injectorSend

local OUTPUT = "blue"
local SUM_LINES = 8
local MAX_STACK = 99
local MINI_DELAY = 5

function list(item)
    if mem.list[item.name] == nil then 
        mem.list[item.name] = { sum = 0, divider = item.count}
        mem.listSize = mem.listSize +1
        
        if mem.listSize > 2 then
            mem.isRunning = false
            digiline_send("debug","Error you got more than 2 material. alloying can only got 2 materials kind")
        end
        
        mem.list[mem.listSize] = item.name
        
        if item.count == mem.divider1 and mem.takeSize1 ~= nil then 
            mem.takeSize1 = nil
        elseif item.count == mem.divider2 then 
            mem.takeSize2 = nil
        end
    end
    mem.list[item.name].sum = mem.list[item.name].sum + item.count
end

function take_next()
    if (mem.isRunning) then
        if mem.takeSize1 ~= nil then
            digiline_send("injector1",{count = mem.takeSize1})
        else
            digiline_send("injector1",{})
        end
        if mem.takeSize2 ~= nil then
            digiline_send("injector2",{count = mem.takeSize2})
        else
            digiline_send("injector2",{})
        end
    end
end

function send_next()
    mem.last_send = mem.last_send or 0
    local clock =  os.clock()
    local diff = clock - mem.last_send
    if diff >= 1.5 then
        mem.last_send = clock 
        local sumToSend1 = mem.list[mem.list[1]].sum
        if sumToSend1 > MAX_STACK then sumToSend1 = MAX_STACK end
        local divider1 =  mem.list[mem.list[1]].divider
        sumToSend1 = math.floor(sumToSend1/divider1) * divider1
        
        local sumToSend2 = mem.list[mem.list[2]].sum
        if sumToSend2 > MAX_STACK then sumToSend2 = MAX_STACK end
        local divider2 =  mem.list[mem.list[2]].divider
        sumToSend2 = math.floor(sumToSend2/divider2) * divider2

        
        if sumToSend1 > 0 and sumToSend2 > 0  then
            local mult1 = sumToSend1/divider1
            local mult2 = sumToSend2/divider2
            
            local mult = mult1
            if mult > mult2 then mult = mult2 end
            
            sumToSend1 = sumToSend1 / mult1 * mult
            sumToSend2 = sumToSend2 / mult2 * mult

            digiline_send("debug",tostring(clock).."sumToSend1 "..tostring(sumToSend1).." sumToSend2 "..tostring(sumToSend2))
            
            digiline_send("injectorSend",{name = mem.list[1],  count = sumToSend1}) 
            mem.list[mem.list[1]].sum = mem.list[mem.list[1]].sum - sumToSend1
            
            digiline_send("injectorSend",{name = mem.list[2],  count = sumToSend2})
            mem.list[mem.list[2]].sum = mem.list[mem.list[2]].sum - sumToSend2
        end
        
        
        --if mem.last == 1 then mem.last_send = clock end -- send the 2 ingredient at the same time
        
    else
        interrupt(1.5 *1.1 - diff ,"SEND_NEXT"..tostring(clock))
    end
    
end

function start() 
    clear()
    mem.isRunning = true
    mem.countSent = 0
    mem.list = {}
    mem.listSize = 0
   -- mem.takenCount = 0
    if mem.divider1 == nil  then
        digiline_send("LCD1", "please choose proportion to start !")
        return
    end
    if mem.divider2 == nil  then
        digiline_send("LCD2", "please choose proportion to start !")
        return
    end
    mem.takeSize1 = mem.divider1
    mem.takeSize2 = mem.divider2
    take_next()
    
    digiline_send("debug","started ...")
end

function clear()
    digiline_send("settings","CLEAR")
    mem.isRunning = nil
    mem.takeSize1 = nil
    --mem.divider1 = nil
    mem.takeSize2 = nil
    --mem.divider2 = nil
    mem.countSent = nil
    mem.list = nil
    mem.listSize = nil
    --mem.takenCount = nil
    mem.last_send = nil
end
function reset() 
    mem.divider1 = nil
    mem.divider2 = nil
    clear()
    digiline_send("LCD1","must enter material1 minimum proportion")
    digiline_send("LCD2","must enter material2 minimum proportion")
    digiline_send("settings","CLEAR")
    digiline_send("debug","reset done!")
end



-- ** EVENTS ** 
if event.type == "item"  then
    list(event.item)
    --mem.takenCount = mem.takenCount + 1
    interrupt(1,"TAKE_NEXT"..tostring(os.clock()))
  --  if mem.takenCount % 2 == 0 then
      --  take_next()
    --digiline_send("debug","sent:"..tostring(mem.countSent).."/"..tostring(SUM_LINES))
    if mem.countSent <= SUM_LINES  then -- first fast send 
        mem.countSent = mem.countSent + 1 
        interrupt(1+MINI_DELAY,"SEND_NEXT"..tostring(os.clock()))
    end
       -- mem.takenCount = 0
  --  end  
elseif event.type == "interrupt" then
    if string.sub(event.iid, 1, 9) == "SEND_NEXT" then
        --digiline_send("debug",tostring(os.clock()).."sending:"..tostring(mem.countSent).."/"..tostring(SUM_LINES))
        send_next()
    elseif string.sub(event.iid, 1, 9) == "TAKE_NEXT" then
        take_next()
    end
elseif event.type == "digiline" then
    local flag = false
    if event.channel == "process" then
        if string.sub(event.msg, 1, 9)  == "SEND_NEXT" then
            send_next()
        end
    elseif  string.sub(event.channel, 1, 8)  ==  "material" then
        if event.msg == "/start" then
            start()
            flag = true
        elseif event.msg == "/reset" then
            reset()
            flag = true
        end
    end
    if not flag and event.channel == "material1" then
        --if event.msg.find("^[0-9]{1,4}$") then
        
        local a = tostring(event.msg)
        local b = "^%d%d?%d?%d?$"
        if a:find(b) then
            mem.divider1 = tonumber(event.msg)
            digiline_send("LCD1","Material1 proportion: "..event.msg)
        else
            digiline_send("LCD1",event.msg.." must be a 4digits max interger")
        end
    elseif not flag and event.channel == "material2" then
        local a = tostring(event.msg)
        local b = "^%d%d?%d?%d?$"
        if a:find(b) then
            mem.divider2 = tonumber(event.msg)
            digiline_send("LCD2","Material2 proportion: "..event.msg)
        else
            digiline_send("LCD2",event.msg.." must be a 4digits max interger")
        end
    end 
elseif event.type == "program" then
    reset()
end

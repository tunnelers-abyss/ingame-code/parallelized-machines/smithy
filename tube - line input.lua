-- ######################################################
--               Smithy input tube 
--         by Guill4um on december, 05th of 2020
-- ######################################################

-- this tube allocate the materials to the machine if it is free
-- else push to the next line


local LINE_NB = 1
local IN = "red"
local NEXT = "white"
local clock = math.floor(os.clock()*100)/100
if event.type == "item"  then
    if event.pin.name == IN then
        return IN
    end
    mem.is_busy = mem.is_busy or 0
    if mem.is_busy < 2 then
        if mem.is_busy == 0 then
            mem.mat1 = event.item
        elseif mem.is_busy == 1 then
            if event.item.name == mem.mat1.name then
                digiline_send("debug2",string.format("%sNEXT%s>%s(%s)",LINE_NB,mem.is_busy,event.item.name,mem.mat1.name))
                return NEXT
            end
        end
        digiline_send("debug2",string.format("%sIN%s>%s(%s)",LINE_NB,mem.is_busy,event.item.name,mem.mat1.name))
        mem.is_busy = mem.is_busy +  1
        return IN
    else
        digiline_send("debug2",string.format("%sNEXT%s>%s(%s)",LINE_NB,mem.is_busy,event.item.name,mem.mat1.name))
        return NEXT
    end
elseif event.type == "digiline" then
    if event.channel == "process" then
        if string.sub(event.msg, 1, 9)  == "SEND_NEXT" then
            local line_nb = tonumber(string.sub(event.msg, 10))
            if line_nb == LINE_NB then
                digiline_send("debug2",string.format("free line %s",line_nb))
                mem.is_busy = 0
            end
        end
    elseif event.channel == "settings" then
        if event.msg == "CLEAR" then
            mem.is_busy = nil
            mem.mat1 = nil
        end
    end
end